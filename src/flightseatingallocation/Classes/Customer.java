/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flightseatingallocation.Classes;

import java.util.Objects;

/**
 *
 * @author James
 */

public class Customer {
    
    //Enums
    public enum Ticket {
        FIRST("First"),
        BUSINESS("Business"),
        ECONOMY("Economy");
        
        private final String label;

        Ticket(String label) {
            this.label = label;
        }

        @Override
        public String toString() {
            return label;
        }
    }
    
    public enum Seat {
        WINDOW("Window"),
        MIDDLE("Middle"),
        AISLE("Aisle");
        
        private final String label;
        
        Seat(String label) {
            this.label = label;
        }

        @Override
        public String toString() {
            return label;
        }
    }
    
    public enum Type {
        ADULT("Adult"),
        CHILD("Child");
        
        private final String label;
        
        Type(String label) {
            this.label = label;
        }

        @Override
        public String toString() {
            return label;
        }
    }
    
    // Attributes
    public String FirstName;
    public String LastName;
    public Ticket ClassType;
    public Seat SeatType;
    public Type TicketType;
    
    // Default Constructor
    public Customer(){}
    
    // Constructor
    public Customer(String fName, String lName, Ticket ticket, Seat seat, Type type)
    {
        FirstName = fName;
        LastName = lName;
        ClassType = ticket;
        SeatType = seat;
        TicketType = type;
    }
    
    public String getFullName()
    {
        return LastName + ", " + FirstName;
    }
    
    // to help remove Customer from list hopefully
    @Override
    public boolean equals(Object obj) {
        return (this.FirstName.equals(((Customer) obj).FirstName) && (this.LastName.equals(((Customer) obj).LastName)));
    }
}
