/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flightseatingallocation.Classes;

import flightseatingallocation.Classes.Customer.Seat;
import flightseatingallocation.Classes.Customer.Ticket;
import flightseatingallocation.Classes.Customer.Type;

/**
 *
 * @author James
 */
public final class Position {
    
    // Enum for grid marker
    public enum Marker {
        ADULT("A"),
        CHILD("C"),
        EMPTY("E");
        
        private final String label;
        
        Marker(String label) {
            this.label = label;
        }

        @Override
        public String toString() {
            return label;
        }
    }
    
    // Attributes
    public Customer Passenger;
    public Ticket ClassType;
    public Seat SeatType;
    
    public Marker Position;
    
    //Default Constructor - I think this still qualifys as default
    public Position(int row, int col)
    {
        Position = Marker.EMPTY;
        ClassType = getTicket(row);
        SeatType = getSeat(col);
    }
    
    // Constructor for seat allocation
    public Position(Customer customer)
    {
        Passenger = customer;
        ClassType = customer.ClassType;
        Position = getMarker();
    }
    
    public Ticket getTicket(int row)
    {
        if(row >= 0 && row <= 3)
        {
                return Ticket.FIRST;          
        }
        else if (row >= 4 && row <= 7)
        {
            return Ticket.BUSINESS;
        }
        else
        {
            return Ticket.ECONOMY;
        }
    }
    
    public Seat getSeat(int col)
    {
        if(col == 0 || col == 5)
        {
                return Seat.WINDOW;          
        }
        else if (col == 1 || col == 4)
        {
            return Seat.MIDDLE;
        }
        else
        {
            return Seat.AISLE;
        }
    }
    
    public Marker getMarker()
    {
        if(Passenger != null)
        {
            if(Passenger.TicketType == Type.ADULT)
            {
                    return Marker.ADULT;          
            }
            else if (Passenger.TicketType == Type.CHILD)
            {
                return Marker.CHILD; 
            }
            else
            {
                return Marker.EMPTY;
            } 
        }
        else
        {
            return Marker.EMPTY;
        } 
    }
    
    public boolean isEmpty()
    {
        return this.Position == Marker.EMPTY;
    }
}
