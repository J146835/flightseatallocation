/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flightseatingallocation;

import flightseatingallocation.Classes.Customer;
import flightseatingallocation.Classes.Customer.Seat;
import flightseatingallocation.Classes.Customer.Ticket;
import flightseatingallocation.Classes.Customer.Type;
import flightseatingallocation.Classes.Position;
import static flightseatingallocation.FlightSeatingAllocation.AllocateSeat;
import static flightseatingallocation.FlightSeatingAllocation.CancelSeat;
import static flightseatingallocation.FlightSeatingAllocation.WriteFile;
import static flightseatingallocation.FlightSeatingAllocation.addCustomer;
import static flightseatingallocation.FlightSeatingAllocation.getSeats;
import static flightseatingallocation.FlightSeatingAllocation.printArray;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javax.swing.JOptionPane;

/**
 *
 * @author James
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    public TextField txtFirstName;
    public TextField txtLastName;
    public TextField txtSearch;
    public TextArea txtGrid;
    public ComboBox<Ticket> cbTicket = new ComboBox<>();
    public ComboBox<Seat> cbSeat = new ComboBox<>();
    public ComboBox<Type> cbType = new ComboBox<>();
    public ListView lvCustomer = new ListView<>();
    
    // Array declaration for grid use
    Position[][] seats;
    
    List<Customer> list = new ArrayList();
    
    // Example method
    @FXML
    private void handleButtonAction(ActionEvent event) {
        System.out.println("You clicked me!");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // sets enum values as combobox values
        cbTicket.getItems().setAll(Ticket.values());
        cbSeat.getItems().setAll(Seat.values());
        cbType.getItems().setAll(Type.values());

        seats = getSeats();
    }
    
    // Button that displays the seat grid
    @FXML
    public void DisplayGrid(ActionEvent event)
    {   
        // Prints the array to textbox
        txtGrid.setText(printArray(seats));
    }
    
    public static void infoBox(String infoMessage, String titleBar)
    {
        JOptionPane.showMessageDialog(null, infoMessage, "InfoBox: " + titleBar, JOptionPane.INFORMATION_MESSAGE);
    }
    
    // Button that creates a customer and allocates a seat
    @FXML
    public void Seat(ActionEvent event)
    {   
        int row = seats.length;
        int col= seats[0].length;
        // I lost hours to this shit
        Customer testCustomer = new Customer(
                txtFirstName.getText(),
                txtLastName.getText(),
                Ticket.valueOf(cbTicket.getValue().toString().toUpperCase().trim()),
                Seat.valueOf(cbSeat.getValue().toString().toUpperCase().trim()),
                Type.valueOf(cbType.getValue().toString().toUpperCase().trim()));
        // Use functions to find seat indexes here
        //seats[5][2] = new Position(testCustomer);
        AllocateSeat(testCustomer, seats, list);
        addCustomer(lvCustomer, list);
        Collections.sort(list, (final Customer object1, final Customer object2) -> object1.LastName.compareTo(object2.LastName));
        

        // Prints the Grid
        txtGrid.setText(printArray(seats));
        
        WriteFile("Seats.txt", seats);
    }
    
        // Button that creates a customer and allocates a seat
    @FXML
    public void Cancel(ActionEvent event)
    {   
        int row = seats.length;
        int col= seats[0].length;
        // I lost hours to this shit
        Customer testCustomer = new Customer(
                txtFirstName.getText(),
                txtLastName.getText(),
                Ticket.valueOf(cbTicket.getValue().toString().toUpperCase().trim()),
                Seat.valueOf(cbSeat.getValue().toString().toUpperCase().trim()),
                Type.valueOf(cbType.getValue().toString().toUpperCase().trim()));
        // Use functions to find seat indexes here
        //seats[5][2] = new Position(testCustomer);
        CancelSeat(testCustomer, seats, list);
        list.remove(testCustomer);
        addCustomer(lvCustomer, list);
        Collections.sort(list, (final Customer object1, final Customer object2) -> object1.LastName.compareTo(object2.LastName));
        // Prints the Grid
        txtGrid.setText(printArray(seats));
        
        WriteFile("Seats.txt", seats);
    }
    
    //Button that searches the list for a specific LastName
    @FXML
    public void Search(ActionEvent event)
    {
        if(!"".equals(txtSearch.getText()))
        {
            List<Customer> result = list.stream().filter(c -> Objects.equals(c.LastName, txtSearch.getText().trim())).collect(Collectors.toList());
            addCustomer(lvCustomer, result);
        }
        else
        {
            addCustomer(lvCustomer, list);
        }   
    }
}
