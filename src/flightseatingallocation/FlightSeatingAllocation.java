/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flightseatingallocation;

import flightseatingallocation.Classes.Customer;
import flightseatingallocation.Classes.Customer.Seat;
import flightseatingallocation.Classes.Position;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.List;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.stage.Stage;

/**
 *
 * @author James
 */
public class FlightSeatingAllocation extends Application {
    
    private static final Position[][] seats = new Position[12][6];
    static int row = seats.length;
    static int col= seats[0].length;
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        
        Scene scene = new Scene(root, 800, 500);
        
        stage.setScene(scene);
        stage.setTitle("Flight Seat Allocation");
        stage.show();
        
        setPositions();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
    public void setPositions()
    {
        for(int i = 0; i<row; i++)
        {
            for(int j = 0; j<col; j++)
            {
                // assigns a new Position to each position in the array
                seats[i][j] = new Position(i,j);
                // For testing if the Ticket types are assigned properly
                //System.out.println(seats[i][j].Ticket.toString());
                //System.out.println(seats[i][j].SeatType.toString());
            }    
        }   
    }
    
    // Use for printing grid to console
    public void printArrayCon()
    {
        for(int i = 0; i < row; i++)
        {
            for(int j = 0; j < col; j++)
            {
                System.out.print("[" + seats[i][j].getMarker() + "] ");
            }
            System.out.println();
        }
    }
    
        // Use for printing grid to textbox
    public static String printArray(Position[][] seats)
    {   
        String output = "";
        for(int i = 0; i < row; i++)
        {
            for(int j = 0; j < col; j++)
            {
                output += "[" + seats[i][j].getMarker() + "] ";
            }
            output += "\n";
        }
        return output;
    }
    

    
    public static Position[][] getSeats()
    {
        return seats;
    }
    
    public static void AllocateSeat(Customer customer, Position[][] arrSeats, List<Customer> list)
    {
        rowloop:
        for(int i = 0; i<row; i++)
        {
            for(int j = 0; j<col; j++)
            {
                //System.out.println(seats[i][j].isEmpty());
                if(arrSeats[i][j].isEmpty())
                {
                    if(arrSeats[i][j].ClassType == customer.ClassType && arrSeats[i][j].SeatType == customer.SeatType)
                    {
                        arrSeats[i][j] = new Position(customer);
                        list.add(customer);
                        
                        System.out.println("Seat Allocated");
                        break rowloop;
                    }
                }
            }    
        }
    }
    
    public static void CancelSeat(Customer customer, Position[][] arrSeats, List<Customer> list)
    {
        rowloop:
        for(int i = 0; i<row; i++)
        {
            for(int j = 0; j<col; j++)
            {
                //System.out.println(seats[i][j].isEmpty());
                if(!arrSeats[i][j].isEmpty())
                {
                    if(arrSeats[i][j].Passenger.FirstName.equals(customer.FirstName) && arrSeats[i][j].Passenger.LastName.equals(customer.LastName))
                    {
                        arrSeats[i][j] = new Position(i,j);
                        System.out.println("Seat Booking Cancelled");
                        break rowloop;
                    }
                }
            }    
        }
    }
    
    public static void addCustomer(ListView lvCustomer, List<Customer> list)
    {
        ObservableList<Customer> myObservableList = FXCollections.observableList(list);
        lvCustomer.setItems(myObservableList);
        lvCustomer.setCellFactory(param -> new ListCell<Customer>() {
            @Override
            protected void updateItem(Customer customer, boolean empty) {
                super.updateItem(customer, empty);

                if (empty || customer == null || customer.getFullName() == null)
                {
                    setText(null);
                } else
                {
                    setText(customer.getFullName());
                }
            }
        });
    }
    
    public static void WriteFile(String fileName, Position[][] seats) 
    {
        try (RandomAccessFile raf = new RandomAccessFile(fileName, "rw")) {
            raf.writeBytes(printArray(seats));
        }
        catch(Exception e)
        {
          // Messagebox here  
        }
  }
}
