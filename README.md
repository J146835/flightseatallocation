# FlightSeatAllocation
For Java Assessment 2

Known Issues:
- There is no notification when seats of a specific type are all taken
- There is no notification when file is written to
- There is no notification of errors (None occured with basic testing)
- Probably other things

Instructions
- Allocate Seat
	- Enter First Name
	- Enter Last Name
	- Select Class Type
	- Select Fare Type
	- Select Seat Type
	- Click Book Seat
	
- Cancel Seat
	- Enter First Name
	- Enter Last Name
	- Select Class Type
	- Select Fare Type
	- Select Seat Type
	- Click Cancel Seat

- Search Customers
	- Type Last Name in Search box
	- Click Search
	- Delete text in Search box and click search to return to orginal listing

- Sort Customers
	- This occurs automatically without input
